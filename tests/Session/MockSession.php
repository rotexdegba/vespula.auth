<?php
namespace Vespula\Auth\Session;

use Vespula\Auth\Session\SessionInterface;
use Vespula\Auth\Auth;

class MockSession implements SessionInterface 
{
    protected $store = [
        'status'=>Auth::ANON
    ];
    protected $idle;
    protected $expire;

    public function __construct($idle = null, $expire = null, $key=null)
    {
        $this->idle = $idle;
        $this->expire = $expire;
    }

    public function getStatus(): ?string
    {
        return $this->store['status'];
    }
    public function setStatus(string $status): void
    {
        $this->store['status'] = $status;
    }

    public function setUsername(string $username): void
    {
        $this->store['username'] = $username;
    }
    public function getUsername(): ?string
    {
        return $this->store['username'];
    }
    public function getUserdata(): ?array
    {
        return $this->store['userdata'];
    }
    public function setUserdata(array $data): void
    {
        $this->store['userdata'] = (array) $data;
    }

    public function isIdled(): bool
    {
        return true;
    }

    public function isExpired(): bool
    {
        return false;
    }

    public function reset(): void
    {
        $this->store['username'] = null;
        $this->store['userdata'] = null;
    }
    
    public function regenerateId(): void
    {
    }
}